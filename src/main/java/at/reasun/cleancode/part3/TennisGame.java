package at.reasun.cleancode.part3;

public interface TennisGame {
    void wonPoint(String playerName);

    String getScore();
}
