RPG Combat Kata
================
Adapted from: [https://github.com/ardalis/kata-catalog](https://github.com/ardalis/kata-catalog)

# Background #

Iteratively build simple combat rules, as for a role-playing game (RPG). 
The domain doesn't include a map or any other character skills apart from their ability to damage and heal one another.

# Instructions #

1. Complete each iteration before reading the next one.
2. Write a failing test, make it pass, commit, refactor as needed, commit, repeat
3. Your goal is to have as many (sensible) commits as possible.

## Iteration One ##

1. All Characters, when created, have:
    - Health, starting at 1000
    - Level, starting at 1
    - May be Alive or Dead, starting Alive (Alive may be a true/false)

1. Characters can Deal Damage to Characters.
    - Damage is subtracted from Health
    - When damage received exceeds current Health, Health becomes 0 and the character dies

1. A Character can Heal a Character.
    - Dead characters cannot be healed
    - Healing cannot raise health above 1000

## Iteration Two ##

1. A Character cannot Deal Damage to itself.

1. A Character can only Heal itself.

1. When dealing damage:
    - If the target is 5 or more Levels above the attacker, Damage is reduced by 50%
    - If the target is 5 or more Levels below the attacker, Damage is increased by 50%

## Iteration Three ##

1. Characters have an attack Max Range.

1. *Melee* fighters have a range of 2 meters.

1. *Ranged* fighters have a range of 20 meters.

1. Characters must be in range to deal damage to a target.

# Resources #

- Original Source: http://www.slideshare.net/DanielOjedaLoisel/rpg-combat-kata
